/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/rectangle_impl.hpp"

#include <cmath>
#include <cstring>
#include <stdexcept>

namespace Ctp {
namespace Shapes {

RectangleImpl::RectangleImpl(Common::Point _topLeftPoint, Common::Point _rightBottomPoint)
{
	if ((_topLeftPoint.m_x > _rightBottomPoint.m_x)
		||	(_topLeftPoint.m_y < _rightBottomPoint.m_y)
		||	(_topLeftPoint == _rightBottomPoint)
	)
		throw std::logic_error{ "Invalid rectangle coordinates" };

	m_xTL = _topLeftPoint.m_x;
	m_yTL = _topLeftPoint.m_y;
	m_xBR = _rightBottomPoint.m_x;
	m_yBR = _rightBottomPoint.m_y;
}

RectangleImpl::RectangleImpl ( Common::Point _point, double _width, double _height )
{
	if ( ( _width <= 0 ) || ( _height <= 0 ) )
		throw std::logic_error{ "Invalid rectangle coordinates" };

	m_xTL = _point.m_x;
	m_yTL = _point.m_y;
	m_xBR = _point.m_x + _width;
	m_yBR = _point.m_y - _height;
}

Common::Point
RectangleImpl::getTopLeft() const
{
	return { m_xTL, m_yTL };
}

Common::Point
RectangleImpl::getTopRight() const
{
	return { m_xBR, m_yTL };
}

Common::Point
RectangleImpl::getBottomLeft() const
{
	return { m_xTL, m_yBR };
}

Common::Point
RectangleImpl::getBottomRight() const
{
	return { m_xBR, m_yBR };
}

double
RectangleImpl::getWidth() const
{
	return std::abs(m_xBR - m_xTL);
}

double
RectangleImpl::getHeight() const
{
	return std::abs(m_yTL - m_yBR);
}

double
RectangleImpl::getPerimeter() const
{
	return (getHeight() + getWidth()) * 2;
}

double
RectangleImpl::getArea() const
{
	return getHeight() * getWidth();
}

bool
RectangleImpl::contains ( Common::Point _point ) const
{
	return (m_xTL <= _point.m_x) && (m_yTL >= _point.m_y)
	    && (m_yBR <= _point.m_y) && (m_xBR >= _point.m_x);
}

bool
RectangleImpl::intersects ( Rectangle const & _other ) const
{
	return contains( _other.getTopLeft() )
	    || contains( _other.getTopRight() )
	    || contains( _other.getBottomLeft() )
	    || contains( _other.getBottomRight() );
}

bool
RectangleImpl::covers ( Rectangle const & _other ) const
{
	return contains( _other.getTopLeft() )
	    && contains( _other.getTopRight() )
	    && contains( _other.getBottomLeft() )
	    && contains( _other.getBottomRight() );
}

} // namespace Shapes
} // namespace Ctp
