# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(shapes
  LANGUAGES
    CXX
  DESCRIPTION
    "Dummy library that implements 2D shapes object."
)

#------------------------------------------------------------------------------#
# Project files                                                                #
#------------------------------------------------------------------------------#

set(API_HEADERS
  include/shapes/accessor.hpp
  include/shapes/rectangle.hpp
  include/shapes/shapes_factory.hpp
)

set(HEADERS
  ${API_HEADERS}

  src/accessor_impl.hpp
  src/rectangle_impl.hpp
  src/shapes_factory_impl.hpp
)

set(SOURCE
  src/accessor_impl.cpp
  src/rectangle_impl.cpp
  src/shapes_factory_impl.cpp
)

#------------------------------------------------------------------------------#
# Project                                                                      #
#------------------------------------------------------------------------------#

add_library("${PROJECT_NAME}" ${HEADERS} ${SOURCE})
add_library(Ctp::Shapes ALIAS ${PROJECT_NAME})

source_group("api" FILES ${API_HEADERS})

target_init("${PROJECT_NAME}")

#------------------------------------------------------------------------------#
# Project directories                                                          #
#------------------------------------------------------------------------------#

target_include_directories("${PROJECT_NAME}"
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    # https://stackoverflow.com/a/13797627
    # https://stackoverflow.com/a/54669846
    $<BUILD_INTERFACE:$<$<BOOL:${ENABLE_TESTING}>:${PROJECT_SOURCE_DIR}>>
    $<INSTALL_INTERFACE:include>
  PRIVATE
    "${PROJECT_SOURCE_DIR}"
)

#------------------------------------------------------------------------------#
# Project precompile headers                                                   #
#------------------------------------------------------------------------------#

target_precompile_headers("${PROJECT_NAME}"
  PRIVATE
    <boost/noncopyable.hpp>
    <memory>
    <string>
    <utility>
)

#------------------------------------------------------------------------------#
# Project libraries                                                            #
#------------------------------------------------------------------------------#

target_link_libraries("${PROJECT_NAME}"
  PUBLIC
    Ctp::Common
  PUBLIC
    Boost::boost
)

#------------------------------------------------------------------------------#
# Project tests                                                                #
#------------------------------------------------------------------------------#

if(ENABLE_TESTING)
  add_subdirectory(tests)
endif()
