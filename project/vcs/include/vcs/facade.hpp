/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 * 
 * Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_VCS_FACADE_HPP
#define CTP_VCS_FACADE_HPP

#include <boost/noncopyable.hpp>

#include <string_view>

namespace Ctp {
namespace Vcs {

// Based on andrew-hardin/cmake-git-version-tracking better example.
class Facade
    :   private boost::noncopyable
{

public:

    virtual ~Facade () = default;

    // Is the metadata populated? We may not have metadata if
    // there wasn't a .git directory (e.g. downloaded source
    // code without revision history).
    [[nodiscard]]
    virtual bool isPopulated () const = 0;

    // Were there any uncommitted changes that won't be reflected
    // in the CommitID?
    [[nodiscard]]
    virtual bool hasUncommittedChanges () const = 0;

    // The commit author's name.
    [[nodiscard]]
    virtual std::string_view getAuthorName () const = 0;

    // The commit author's email.
    [[nodiscard]]
    virtual std::string_view getAuthorEmail () const = 0;

    // The commit SHA1.
    [[nodiscard]]
    virtual std::string_view getCommitSha1 () const = 0;

    // The ISO8601 commit date.
    [[nodiscard]]
    virtual std::string_view getCommitDate () const = 0;

    // The commit subject.
    [[nodiscard]]
    virtual std::string_view getCommitSubject () const = 0;

    // The commit body.
    [[nodiscard]]
    virtual std::string_view getCommitBody () const = 0;

    // The commit describe.
    [[nodiscard]]
    virtual std::string_view getDescribe () const = 0;

};

} // namespace Vcs
} // namespace Ctp

#endif // CTP_VCS_FACADE_HPP
