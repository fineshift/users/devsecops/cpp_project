/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_VCS_SRC_GITFACADEIMPL_HPP
#define CTP_VCS_SRC_GITFACADEIMPL_HPP

#include "vcs/facade.hpp"

namespace Ctp {
namespace Vcs {

class GitFacadeImpl final
    :   public Facade
{

public:

    bool isPopulated () const noexcept override;


    bool hasUncommittedChanges () const noexcept override;


    std::string_view getAuthorName () const noexcept override;

    std::string_view getAuthorEmail () const noexcept override;


    std::string_view getCommitSha1 () const noexcept override;

    std::string_view getCommitDate () const noexcept override;

    std::string_view getCommitSubject () const noexcept override;

    std::string_view getCommitBody () const noexcept override;


    std::string_view getDescribe () const noexcept override;

};

} // namespace Vcs
} // namespace Ctp

#endif // CTP_VCS_SRC_GITFACADEIMPL_HPP
