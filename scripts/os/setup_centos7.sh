#!/usr/bin/env bash

set -e

#------------------------------------------------------------------------------#
# Official Red Hat Enterprise Linux RPM software packages                      #
#------------------------------------------------------------------------------#

INSTALL_PKGS=(
    "cmake3-3.17.3"
    "cppcheck-1.90"
    "cppcheck-htmlreport-1.90"
    "devtoolset-8-gcc-8.2.1"
    "devtoolset-8-gcc-c++-8.2.1"
    "devtoolset-8-gcc-gfortran-8.2.1"
    "devtoolset-8-gdb-8.2"
    "devtoolset-8-make-4.2.1"
    "llvm-toolset-7.0-clang-7.0.1"
    "llvm-toolset-7.0-clang-tools-extra"
)

# Install packages

yum install -y centos-release-scl-rh epel-release
yum install -y --setopt=tsflags=nodocs ${INSTALL_PKGS[@]}
rpm -V ${INSTALL_PKGS[@]}
yum -y clean all --enablerepo='*'

# Force system to use CMake3
# https://stackoverflow.com/a/48842999

#ln -s /usr/bin/cmake3 /usr/bin/cmake
#ln -s /usr/bin/ctest3 /usr/bin/ctest

alternatives --install /usr/local/bin/cmake cmake /usr/bin/cmake 10 \
--slave /usr/local/bin/ctest ctest /usr/bin/ctest \
--slave /usr/local/bin/cpack cpack /usr/bin/cpack \
--slave /usr/local/bin/ccmake ccmake /usr/bin/ccmake \
--family cmake

alternatives --install /usr/local/bin/cmake cmake /usr/bin/cmake3 20 \
--slave /usr/local/bin/ctest ctest /usr/bin/ctest3 \
--slave /usr/local/bin/cpack cpack /usr/bin/cpack3 \
--slave /usr/local/bin/ccmake ccmake /usr/bin/ccmake3 \
--family cmake

#------------------------------------------------------------------------------#
# Python                                                                       #
#------------------------------------------------------------------------------#

pip3 install conan
